package com.pos.inventorymanagementservice.audit;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter(AccessLevel.PROTECTED)
@Setter(AccessLevel.PROTECTED)
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable<U> {

    /**
     * inventory created date.
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @CreatedDate
    @Column(name = "created_date", updatable = false, nullable = false)
    private Date createdDate;

    /**
     * inventory updated date.
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @LastModifiedDate
    @Column(name = "updatedDate")
    private Date updatedDate;

}
