package com.pos.inventorymanagementservice.audit;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter(AccessLevel.PROTECTED)
@Setter(AccessLevel.PROTECTED)
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class TransactionAuditable<U> {

    /**
     * transaction date.
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @CreatedDate
    @Column(name = "transaction_date", updatable = false, nullable = false)
    private Date transactionDate;

}
