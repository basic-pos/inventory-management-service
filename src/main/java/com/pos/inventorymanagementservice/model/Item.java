package com.pos.inventorymanagementservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.ManyToOne;
import javax.persistence.CascadeType;
import java.io.Serializable;

@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "item", schema = "public")
public class Item implements Serializable {

    /**
     * item id.
     */
    @Id
    @Column(name = "id", updatable = false, nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * item name.
     */
    @Column(name = "item_name")
    private String itemName;

    /**
     * item description.
     */
    @Column(name = "item_description")
    private String itemDescription;

    /**
     * price.
     */
    @Column(name = "price")
    private Float price;

    /**
     * item category.
     */
    @JoinColumn(name = "item_category_id")
    @JsonBackReference(value = "itemCategoryRef")
    @ManyToOne
    private ItemCategory itemCategory;

    /**
     * inventory.
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonManagedReference(value = "itemRef")
    @OneToOne(mappedBy = "item",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Inventory inventory;
}
