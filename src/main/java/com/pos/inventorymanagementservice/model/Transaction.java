package com.pos.inventorymanagementservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.pos.inventorymanagementservice.audit.TransactionAuditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Data
@NoArgsConstructor
@Entity
@Table(name = "transaction", schema = "public")
public class Transaction extends TransactionAuditable<String>
        implements Serializable {

    /**
     * transaction id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    /**
     * transaction amount.
     */
    @Column(name = "transaction_amount")
    private Float transactionAmount;

    /**
     * payment method.
     */
    @Column(name = "payment_method")
    private String paymentMethod;

    /**
     * transaction status.
     */
    @Column(name = "transaction_status")
    private String transactionStatus;

    /**
     * transaction qty.
     */
    @Column(name = "qty")
    private Integer qty;

    /**
     * order id.
     */
    @Column(name = "order_id")
    private Long orderId;

    /**
     * item.
     */
    @JoinColumn(name = "item_id")
    @ManyToOne
    @JsonBackReference
    private Item item;

}
