package com.pos.inventorymanagementservice.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.pos.inventorymanagementservice.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "inventory", schema = "public")
public class Inventory extends Auditable<String> implements Serializable {

    /**
     * inventory id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false, unique = true)
    private Long id;

    /**
     * qty.
     */
    @Column(name = "qty")
    private Integer qty;

    /**
     * barcode.
     */
    @Column(name = "bar_code")
    private String barCode;

    /**
     * last updated qty.
     */
    @Column(name = "last_updated_qty")
    private Integer lastUpdatedQty;

    /**
     * items.
     */
    @JoinColumn(name = "item_id")
    @OneToOne
    @JsonBackReference("itemRef")
    private Item item;
}

