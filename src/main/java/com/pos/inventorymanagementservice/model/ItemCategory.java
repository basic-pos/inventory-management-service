package com.pos.inventorymanagementservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
@NoArgsConstructor
@Data
@Entity
@Table(name = "item_category", schema = "public")
public class ItemCategory {


    /**
     * item category id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false, unique = true)
    private Long id;

    /**
     * item category name.
     */
    @Column(name = "item_category_name")
    private String itemCategoryName;

    /**
     * item category type.
     */
    @Column(name = "item_type")
    private String itemType;

    /**
     * items.
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonManagedReference(value = "itemCategoryRef")
    @OneToMany(mappedBy = "itemCategory",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<Item> items = new ArrayList<>();
}
