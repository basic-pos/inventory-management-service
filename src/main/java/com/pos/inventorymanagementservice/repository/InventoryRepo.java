package com.pos.inventorymanagementservice.repository;

import com.pos.inventorymanagementservice.model.Inventory;
import com.pos.inventorymanagementservice.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InventoryRepo extends JpaRepository<Inventory, Long> {

    /***
     * find particular inventory by item to update.
     * @param item Item object
     * @return particular inventory
     */
    Inventory findByItem(Item item);
}
