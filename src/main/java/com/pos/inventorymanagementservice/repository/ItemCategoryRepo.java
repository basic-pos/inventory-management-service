package com.pos.inventorymanagementservice.repository;

import com.pos.inventorymanagementservice.model.ItemCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemCategoryRepo extends JpaRepository<ItemCategory, Long> {
}
