package com.pos.inventorymanagementservice.repository;

import com.pos.inventorymanagementservice.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepo extends JpaRepository<Transaction, Long> {
}
