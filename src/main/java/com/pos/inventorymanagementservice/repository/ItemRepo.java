package com.pos.inventorymanagementservice.repository;

import com.pos.inventorymanagementservice.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepo extends JpaRepository<Item, Long> {
}
