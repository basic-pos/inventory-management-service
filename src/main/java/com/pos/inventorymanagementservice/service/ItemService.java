package com.pos.inventorymanagementservice.service;

import com.pos.inventorymanagementservice.model.Item;

import java.util.List;

public interface ItemService {

    /***
     * get all existing items.
     * @return all existing items
     */
    List<Item> getItems();

    /***
     * create new item.
     * @param createItemData new item data to be added to database
     * @return created item
     */
    Item createItem(Item createItemData);

    /***
     * get particular item by id.
     * @param id item id
     * @return particular item
     */
    Item getItem(Long id);

    /***
     * update particular item.
     * @param id item id
     * @param updateItemData new item data to be updated
     * @return updated item
     */
    Item updateItem(Long id, Item updateItemData);
}
