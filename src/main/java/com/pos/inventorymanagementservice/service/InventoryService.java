package com.pos.inventorymanagementservice.service;

import com.pos.inventorymanagementservice.model.Inventory;

import java.util.List;

public interface InventoryService {

    /***
     * get all existing customers.
     * @return all customers
     */
    List<Inventory> getInventories();

    /***
     * create new customer.
     * @param createInventoryData new customer data tobe added
     *                            to customer table.
     * @return created customer
     */
    Inventory createInventory(Inventory createInventoryData);


    /***
     * get particular customer by customer id.
     * @param id customer id
     * @return particular customer
     */
    Inventory getInventory(Long id);


    /***
     * update particular customer.
     * @param id customer id
     * @param updateInventoryData update customer data to be added
     *                            to customer table.
     * @return updated customer
     */
    Inventory updateInventory(Long id, Inventory updateInventoryData);
}
