package com.pos.inventorymanagementservice.service.impl;

import com.pos.inventorymanagementservice.dto.feign.OrderDto;
import com.pos.inventorymanagementservice.dto.rqst.SaleRequestDto;
import com.pos.inventorymanagementservice.exception.OrderNotFoundException;
import com.pos.inventorymanagementservice.model.Inventory;
import com.pos.inventorymanagementservice.model.Item;
import com.pos.inventorymanagementservice.model.Transaction;
import com.pos.inventorymanagementservice.proxy.CustomerOrderManagementServiceProxy;
import com.pos.inventorymanagementservice.repository.InventoryRepo;
import com.pos.inventorymanagementservice.repository.ItemRepo;
import com.pos.inventorymanagementservice.repository.TransactionRepo;
import com.pos.inventorymanagementservice.service.SaleService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Log4j2
@Service
public class SaleServiceImpl implements SaleService {

    /***
     * dependency injection (SaleServiceImpl depends on
     * CustomerOrderManagementServiceProxy).
     */
    @Autowired
    private CustomerOrderManagementServiceProxy proxy;
    /***
     * dependency injection (SaleServiceImpl depends on InventoryRepo).
     */
    @Autowired
    private InventoryRepo inventoryRepo;
    /***
     * dependency injection (SaleServiceImpl depends on TransactionRepo).
     */
    @Autowired
    private TransactionRepo transactionRepo;
    /***
     * dependency injection (SaleServiceImpl depends on ItemRepo).
     */
    @Autowired
    private ItemRepo itemRepo;

    /***
     * create new transaction & update inventory.
     * @param saleData sale data for create transaction
     * @return transaction id
     */
    @Override
    public Long createTransaction(final SaleRequestDto saleData) {

        //get order details using feign API call for customer-order-service
        Long orderId = saleData.getOrderId();
        OrderDto orderDetails = proxy.getOrder(orderId);
        if (orderDetails == null) {
            throw new OrderNotFoundException("can't find order for this id", 450);
        }

        Float amount = orderDetails.getPrice();
        String paymentMethod = orderDetails.getPaymentType();
        Integer qty = orderDetails.getQty();
        Long itemId = orderDetails.getItemId();

        //make transaction
        Transaction transactionDetails = new Transaction();
        transactionDetails.setTransactionAmount(amount);
        transactionDetails.setPaymentMethod(paymentMethod);
        transactionDetails.setTransactionStatus("success");
        transactionDetails.setOrderId(orderId);
        transactionDetails.setQty(qty);
        Item orderedItem = itemRepo.findById(itemId).orElseThrow();
        transactionDetails.setItem(orderedItem);
        Transaction savedTransaction = transactionRepo.save(transactionDetails);

        //update inventory details relevant to transaction
        Inventory inventoryDetails = inventoryRepo.findByItem(orderedItem);
        Integer currentQty = inventoryDetails.getQty();
        Integer newQty = currentQty - qty;
        inventoryDetails.setQty(newQty);
        inventoryDetails.setLastUpdatedQty(qty);
        inventoryRepo.save(inventoryDetails);

        //return transaction id
        Long transactionId = savedTransaction.getId();
        return transactionId;
    }
}
