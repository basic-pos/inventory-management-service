package com.pos.inventorymanagementservice.service.impl;

import com.pos.inventorymanagementservice.exception.ItemCategoryNotFoundException;
import com.pos.inventorymanagementservice.model.ItemCategory;
import com.pos.inventorymanagementservice.repository.ItemCategoryRepo;
import com.pos.inventorymanagementservice.service.ItemCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemCategoryServiceImpl implements ItemCategoryService {

    /***
     * dependency injection (ItemCategoryServiceImpl depends
     * on ItemCategoryRepo).
     */
    @Autowired
    private ItemCategoryRepo itemCategoryRepo;

    /***
     * get all existing item categories in th database.
     * @return all item categories
     */
    @Override
    public List<ItemCategory> getItemCategories() {
        return itemCategoryRepo.findAll();
    }

    /***
     * create new item category.
     * @param createItemCategoryData new item category data
     *                               to be added to database
     * @return created item category
     */
    @Override
    public ItemCategory createItemCategory(final ItemCategory createItemCategoryData) {
        return itemCategoryRepo.save(createItemCategoryData);
    }

    /***
     * get particular item category by id.
     * @param id item category id
     * @return particular item category
     */
    @Override
    public ItemCategory getItemCategory(final Long id) {
        return itemCategoryRepo.findById(id)
                .orElseThrow(() -> new ItemCategoryNotFoundException(
                        "can't find item category for this id", 450));
    }

    /***
     * update particular item category.
     * @param id item category id
     * @param updateItemCategoryData new item category data to be updated
     * @return updated item category
     */
    @Override
    public ItemCategory updateItemCategory(final Long id,
                                           final ItemCategory updateItemCategoryData) {
        return itemCategoryRepo.findById(id)
                .map(itemCategory -> itemCategoryRepo.save(updateItemCategoryData))
                .orElseThrow(() -> new ItemCategoryNotFoundException("can't find item category for this id", 450));
    }
}
