package com.pos.inventorymanagementservice.service.impl;

import com.pos.inventorymanagementservice.exception.InventoryNotFoundException;
import com.pos.inventorymanagementservice.model.Inventory;
import com.pos.inventorymanagementservice.repository.InventoryRepo;
import com.pos.inventorymanagementservice.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InventoryServiceImpl implements InventoryService {

    /***
     * dependency injection (InventoryServiceImpl depends on InventoryRepo).
     */
    @Autowired
    private InventoryRepo inventoryRepo;

    /***
     * get all existing customers.
     * @return all customers
     */
    @Override
    public List<Inventory> getInventories() {
        return inventoryRepo.findAll();
    }

    /***
     * create new customer.
     * @param createInventoryData new customer data tobe added
     *                            to customer table.
     * @return created customer
     */
    @Override
    public Inventory createInventory(final Inventory createInventoryData) {
        Integer qty = createInventoryData.getQty();
        createInventoryData.setLastUpdatedQty(qty);
        return inventoryRepo.save(createInventoryData);
    }

    /***
     * get particular customer by customer id.
     * @param id customer id
     * @return particular customer
     */
    @Override
    public Inventory getInventory(final Long id) {
        return inventoryRepo.findById(id)
                .orElseThrow(() -> new InventoryNotFoundException(
                        "can't find inventory for this id", 450));
    }

    /***
     * update particular customer.
     * @param id customer id
     * @param updateInventoryData update customer data to
     *                            be added to customer table.
     * @return updated customer
     */
    @Override
    public Inventory updateInventory(final Long id,
                                     final Inventory updateInventoryData) {

        return inventoryRepo.findById(id)
                .map(inventory -> {
                    Integer currentQty = inventory.getQty();
                    Integer updateQty = updateInventoryData.getQty();
                    Integer newQty = currentQty + updateQty;
                    updateInventoryData.setQty(newQty);
                    updateInventoryData.setLastUpdatedQty(updateQty);
                    return inventoryRepo.save(updateInventoryData);
                })
                .orElseThrow(() -> new InventoryNotFoundException(
                        "can't find inventory for this id", 450));
    }
}
