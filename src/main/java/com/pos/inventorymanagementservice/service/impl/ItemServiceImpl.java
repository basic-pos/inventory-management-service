package com.pos.inventorymanagementservice.service.impl;

import com.pos.inventorymanagementservice.exception.ItemNotFoundException;
import com.pos.inventorymanagementservice.model.Item;
import com.pos.inventorymanagementservice.repository.ItemRepo;
import com.pos.inventorymanagementservice.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    /***
     * dependency injection (ItemServiceImpl depends on ItemRepo).
     */
    @Autowired
    private ItemRepo itemRepo;

    /***
     * get all existing items.
     * @return all existing items
     */
    @Override
    public List<Item> getItems() {
        return itemRepo.findAll();
    }

    /***
     * create new item.
     * @param createItemData new item data to be added to database
     * @return created item
     */
    @Override
    public Item createItem(final Item createItemData) {
        return itemRepo.save(createItemData);
    }

    /***
     * get particular item by id.
     * @param id item id
     * @return particular item
     */
    @Override
    public Item getItem(final Long id) {
        return itemRepo.findById(id)
                .orElseThrow(() -> new ItemNotFoundException("can't find item for this id", 450));
    }

    /***
     * update particular item.
     * @param id item id
     * @param updateItemData new item data to be updated
     * @return updated item
     */
    @Override
    public Item updateItem(final Long id, final Item updateItemData) {
        return itemRepo.findById(id)
                .map(item -> itemRepo.save(updateItemData))
                .orElseThrow(() -> new ItemNotFoundException("can't find item for this id", 450));
    }
}

