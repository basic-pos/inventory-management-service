package com.pos.inventorymanagementservice.service;

import com.pos.inventorymanagementservice.dto.rqst.SaleRequestDto;

public interface SaleService {

    /***
     * create new transaction.
     * @param saleData sale data for create transaction
     * @return transaction id
     */
    Long createTransaction(SaleRequestDto saleData);
}
