package com.pos.inventorymanagementservice.service;

import com.pos.inventorymanagementservice.model.ItemCategory;

import java.util.List;

public interface ItemCategoryService {

    /***
     * get all existing item categories in th database.
     * @return all item categories
     */
    List<ItemCategory> getItemCategories();

    /***
     * create new item category.
     * @param createItemCategoryData new item category data
     *                               to be added to database
     * @return created item category
     */
    ItemCategory createItemCategory(ItemCategory createItemCategoryData);

    /***
     * get particular item category by id.
     * @param id item category id
     * @return particular item category
     */
    ItemCategory getItemCategory(Long id);

    /***
     * update particular item category.
     * @param id item category id
     * @param updateItemCategoryData new item category data to be updated
     * @return updated item category
     */
    ItemCategory updateItemCategory(Long id,
                                    ItemCategory updateItemCategoryData);
}
