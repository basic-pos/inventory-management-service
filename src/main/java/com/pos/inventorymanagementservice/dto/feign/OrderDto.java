package com.pos.inventorymanagementservice.dto.feign;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OrderDto {


    /**
     * order id.
     */
    private Long id;

    /**
     * item id.
     */
    private Long itemId;

    /**
     * price.
     */
    private Float price;

    /**
     * qty.
     */
    private Integer qty;

    /**
     * payment type.
     */
    private String paymentType;


}
