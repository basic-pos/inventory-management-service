package com.pos.inventorymanagementservice.dto.rqst;

import com.pos.inventorymanagementservice.model.Inventory;
import com.pos.inventorymanagementservice.model.ItemCategory;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class ItemRequestDto {

    public static final int maxItemNameSize = 50;
    public static final int minItemDescriptionSize = 20;
    public static final int maxItemDescriptionSize = 100;
    public static final int maxItemCategorySize = 20;

    /**
     * item name.
     */
    @NotNull(message = "item name can't be null")
    @Max(value = maxItemNameSize, message = "item name must be less than 50 characters")
    private String itemName;

    /**
     * item description.
     */
    @NotNull(message = "item description can't be null")
    @Size(min = minItemDescriptionSize, max = maxItemDescriptionSize,
            message = "item description should be between 10 and 100 characters")
    private String itemDescription;

    /**
     * price.
     */
    @NotNull(message = "item price can't be null")
    @Pattern(regexp = "(^[0-9]{10})")
    private String price;

    /**
     * item category.
     */
    @NotNull(message = "item category can't be null")
    @Max(value = maxItemCategorySize, message = "item category must be less than 20 characters")
    private ItemCategory itemCategory;

    /**
     * inventory.
     */
    private Inventory inventory;

}
