package com.pos.inventorymanagementservice.dto.rqst;

import com.pos.inventorymanagementservice.model.Item;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class InventoryRequestDto {

    /**
     * qty.
     */
    @NotNull(message = "item qty can't be null")
    @Pattern(regexp = "(^[0-9]{10})")
    private String qty;

    /**
     * barcode.
     */
    @NotNull(message = "item barcode can't be null")
    private String barCode;

    /**
     * items.
     */
    @NotNull(message = "item can't be null")
    private Item item;

}
