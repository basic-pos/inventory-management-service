package com.pos.inventorymanagementservice.dto.rqst;

import com.pos.inventorymanagementservice.model.Item;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class ItemCategoryRequestDto {

    public static final int maxItemCategoryNameSize = 50;
    public static final int maxItemCategoryTypeSize = 50;

    /**
     * item category name.
     */
    @NotNull(message = "item category name can't be null")
    @Max(value = maxItemCategoryNameSize, message = "item category name must be less than 50 characters")
    private String itemCategoryName;

    /**
     * item category type.
     */
    @NotNull(message = "item type can't be null")
    @Max(value = maxItemCategoryTypeSize, message = "item category type must be less than 50 characters")
    private String itemType;

    /**
     * items.
     */
    private List<Item> items = new ArrayList<>();
}
