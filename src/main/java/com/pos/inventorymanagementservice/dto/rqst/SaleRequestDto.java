package com.pos.inventorymanagementservice.dto.rqst;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
public class SaleRequestDto {

    /**
     * order id.
     */
    @NotNull(message = "order id can't be null")
    private Long orderId;

}
