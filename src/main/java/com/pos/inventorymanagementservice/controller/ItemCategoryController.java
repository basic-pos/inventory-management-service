package com.pos.inventorymanagementservice.controller;

import com.pos.inventorymanagementservice.dto.rqst.ItemCategoryRequestDto;
import com.pos.inventorymanagementservice.model.ItemCategory;
import com.pos.inventorymanagementservice.service.ItemCategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@Validated
@RequestMapping(value = "/item-categories")
public class ItemCategoryController {

    /***
     * dependency injection (ItemCategoryController depends on
     * ItemCategoryService).
     */
    @Autowired
    private ItemCategoryService itemCategoryService;

    /***
     * get all existing item categories in th database.
     * @return all item categories
     */
    @GetMapping
    public List<ItemCategory> getItemCategories() {
        return itemCategoryService.getItemCategories();
    }

    /***
     * create new item category.
     * @param createItemCategoryData new item category data to be added
     *                               to database
     * @return created item category
     */
    @PostMapping
    public ItemCategory createItemCategory(@Valid @RequestBody final
                                           ItemCategoryRequestDto createItemCategoryData) {
        ModelMapper modelMapper = new ModelMapper();
        ItemCategory persistenceItemCategoryData = modelMapper.map(createItemCategoryData, ItemCategory.class);
        return itemCategoryService.createItemCategory(persistenceItemCategoryData);
    }

    /***
     * get particular item category by id.
     * @param id item category id
     * @return particular item category
     */
    @GetMapping(value = "/{id}")
    public ItemCategory getItemCategory(@PathVariable(value = "id") @Min(1) final Long id) {
        return itemCategoryService.getItemCategory(id);
    }

    /***
     * update particular item category.
     * @param id item category id
     * @param updateItemCategoryData new item category data to be updated
     * @return updated item category
     */
    @PutMapping(value = "/{id}")
    public ItemCategory updateItemCategory(@PathVariable(value = "id") @Min(1) final Long id,
                                           @Valid @RequestBody final ItemCategoryRequestDto updateItemCategoryData) {
        ModelMapper modelMapper = new ModelMapper();
        ItemCategory persistenceItemCategoryData = modelMapper.map(updateItemCategoryData, ItemCategory.class);
        return itemCategoryService.updateItemCategory(id, persistenceItemCategoryData);
    }
}
