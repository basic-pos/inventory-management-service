package com.pos.inventorymanagementservice.controller;

import com.pos.inventorymanagementservice.dto.rqst.ItemRequestDto;
import com.pos.inventorymanagementservice.model.Item;
import com.pos.inventorymanagementservice.service.ItemService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@Validated
@RequestMapping(value = "/items")
public class ItemController {

    /***
     * dependency injection (ItemController depends on ItemService).
     */
    @Autowired
    private ItemService itemService;

    /***
     * get all existing items.
     * @return all existing items
     */
    @GetMapping
    public List<Item> getItems() {
        return itemService.getItems();
    }

    /***
     * create new item.
     * @param createItemData new item data to be added to database
     * @return created item
     */
    @PostMapping
    public Item createItem(@Valid @RequestBody final ItemRequestDto createItemData) {
        ModelMapper modelMapper = new ModelMapper();
        Item persistenceItemData = modelMapper.map(createItemData, Item.class);
        return itemService.createItem(persistenceItemData);
    }

    /***
     * get particular item by id.
     * @param id item id
     * @return particular item
     */
    @GetMapping(value = "/{id}")
    public Item getItem(@PathVariable(value = "id") @Min(1) final Long id) {
        return itemService.getItem(id);
    }

    /***
     * update particular item.
     * @param id item id
     * @param updateItemData new item data to be updated
     * @return updated item
     */
    @PutMapping(value = "/{id}")
    public Item updateItem(@PathVariable(value = "id") @Min(1) final Long id,
                           @Valid @RequestBody final ItemRequestDto updateItemData) {
        ModelMapper modelMapper = new ModelMapper();
        Item persistenceItemData = modelMapper.map(updateItemData, Item.class);
        return itemService.updateItem(id, persistenceItemData);
    }
}
