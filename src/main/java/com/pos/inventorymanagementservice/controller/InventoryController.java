package com.pos.inventorymanagementservice.controller;

import com.pos.inventorymanagementservice.dto.rqst.InventoryRequestDto;
import com.pos.inventorymanagementservice.model.Inventory;
import com.pos.inventorymanagementservice.repository.InventoryRepo;
import com.pos.inventorymanagementservice.service.InventoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@Validated
@RequestMapping(value = "/inventories")
public class InventoryController {

    /***
     * dependency injection(InventoryController depends on InventoryService.
     */
    @Autowired
    private InventoryService inventoryService;

    /***
     * get all existing customers.
     * @return all customers
     */
    @GetMapping
    public List<Inventory> getInventories() {
        return inventoryService.getInventories();
    }

    /***
     * create new customer.
     * @param createInventoryData new customer data tobe
     *                            added to customer table.
     * @return created customer
     */
    @PostMapping
    public Inventory createInventory(@Valid @RequestBody final InventoryRequestDto createInventoryData) {
        ModelMapper modelMapper = new ModelMapper();
        Inventory persistenceInventoryData = modelMapper.map(createInventoryData, Inventory.class);
        return inventoryService.createInventory(persistenceInventoryData);
    }

    /***
     * get particular customer by customer id.
     * @param id customer id
     * @return particular customer
     */
    @GetMapping(value = "/{id}")
    public Inventory getInventory(@PathVariable(value = "id") @Min(1) final Long id) {
        return inventoryService.getInventory(id);
    }

    /***
     * update particular customer.
     * @param id customer id
     * @param updateInventoryData update customer data to be added
     *                            to customer table.
     * @return updated customer
     */
    @PutMapping(value = "/{id}")
    public Inventory updateInventory(@PathVariable(value = "id") final Long id,
                                     @Valid @RequestBody final InventoryRequestDto updateInventoryData) {
        ModelMapper modelMapper = new ModelMapper();
        Inventory persistenceInventoryData = modelMapper.map(updateInventoryData, Inventory.class);
        return inventoryService.updateInventory(id, persistenceInventoryData);
    }

}
