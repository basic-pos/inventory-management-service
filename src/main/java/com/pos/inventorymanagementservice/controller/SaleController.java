package com.pos.inventorymanagementservice.controller;

import com.pos.inventorymanagementservice.dto.rqst.SaleRequestDto;
import com.pos.inventorymanagementservice.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/sale")
public class SaleController {

    /***
     * dependency injection (SaleController depends on SaleService).
     */
    @Autowired
    private SaleService saleService;

    /***
     * create new transaction.
     * @param saleData sale data for create transaction.
     * @return transaction id
     */
    @PostMapping
    public Long createTransaction(@Valid @RequestBody final SaleRequestDto saleData) {
        return saleService.createTransaction(saleData);
    }

}
