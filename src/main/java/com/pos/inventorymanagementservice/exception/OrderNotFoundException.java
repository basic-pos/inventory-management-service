package com.pos.inventorymanagementservice.exception;

import lombok.Data;

@Data
public class OrderNotFoundException extends RuntimeException{

    private String message;
    private int status;

    public OrderNotFoundException(String message, int status) {
        super(message);
        this.message = message;
        this.status = status;
    }
}
