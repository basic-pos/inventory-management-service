package com.pos.inventorymanagementservice.exception;

import lombok.Data;

@Data
public class InventoryNotFoundException extends RuntimeException {

    private String message;
    private int status;

    public InventoryNotFoundException(String message, int status) {
        super(message);
        this.message = message;
        this.status = status;
    }
}
