package com.pos.inventorymanagementservice.exception.handler;

import com.pos.inventorymanagementservice.exception.InventoryNotFoundException;
import com.pos.inventorymanagementservice.exception.ItemCategoryNotFoundException;
import com.pos.inventorymanagementservice.exception.ItemNotFoundException;
import com.pos.inventorymanagementservice.exception.OrderNotFoundException;
import javassist.NotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Log4j2
@ControllerAdvice
public class AppExceptionHandler {

    static final String MSG = "exception found: {}";

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleException(Exception exception) {
        log.error(MSG, exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {NotFoundException.class})
    public ResponseEntity<Object> handleNotFoundException(NotFoundException exception) {
        log.error(MSG, exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {InventoryNotFoundException.class})
    public ResponseEntity<Object> handleInventoryNotFoundException(InventoryNotFoundException exception) {
        log.error(MSG, exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {ItemCategoryNotFoundException.class})
    public ResponseEntity<Object> handleItemCategoryNotFoundException(ItemCategoryNotFoundException exception) {
        log.error(MSG, exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {ItemNotFoundException.class})
    public ResponseEntity<Object> handleItemNotFoundException(ItemNotFoundException exception) {
        log.error(MSG, exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {OrderNotFoundException.class})
    public ResponseEntity<Object> handleOrderNotFoundException(OrderNotFoundException exception) {
        log.error(MSG, exception.getMessage());
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }

}
