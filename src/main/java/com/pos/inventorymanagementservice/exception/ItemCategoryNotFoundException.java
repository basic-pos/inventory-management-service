package com.pos.inventorymanagementservice.exception;

import lombok.Data;

@Data
public class ItemCategoryNotFoundException extends RuntimeException {

    private String message;
    private int status;

    public ItemCategoryNotFoundException(String message, int status) {
        super(message);
        this.message = message;
        this.status = status;
    }
}
