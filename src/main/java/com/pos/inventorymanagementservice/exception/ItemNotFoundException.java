package com.pos.inventorymanagementservice.exception;

import lombok.Data;

@Data

public class ItemNotFoundException extends RuntimeException {

    private String message;
    private int status;

    public ItemNotFoundException(String message, int status) {
        super(message);
        this.message = message;
        this.status = status;
    }
}
