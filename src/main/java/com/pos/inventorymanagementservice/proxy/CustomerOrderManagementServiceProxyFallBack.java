package com.pos.inventorymanagementservice.proxy;

import com.pos.inventorymanagementservice.dto.feign.OrderDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class CustomerOrderManagementServiceProxyFallBack
        implements CustomerOrderManagementServiceProxy {

    /***
     * proxy fallback.
     * @param id order id
     * @return null
     */
    @Override
    public OrderDto getOrder(final Long id) {
        log.info("unable to call customer-order-management service");
        return null;
    }
}
