package com.pos.inventorymanagementservice.proxy;

import com.pos.inventorymanagementservice.dto.feign.OrderDto;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "customer-order-management-service")
@RibbonClient(name = "customer-order-management-service")
public interface CustomerOrderManagementServiceProxy {

    /***
     * get order details by api calling to customer-order-management-service
     * using feign.
     * @param id order id
     * @return order
     */
    @GetMapping(value = "/orders/{id}")
    OrderDto getOrder(@PathVariable(value = "id") Long id);

}
